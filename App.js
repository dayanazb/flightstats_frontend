import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import Login from './components/Login';
import HomeScreen from './screens/HomeScreen';

const RootStack = createStackNavigator(
  {
    Login: { screen: Login },
    Home: { screen: HomeScreen },
  },
  {
      initialRouteName: 'Login',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
