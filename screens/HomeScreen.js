import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import List from '../components/List';

export default class HomeScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: [],
    }
  }

  static navigationOptions = {
    title: "HomeScreen",
    headerStyle: {
      backgroundColor: "#E91E63"
    }
  };

  componentDidMount(){
    fetch('http://aviation-edge.com/v2/public/flights?key=4ebde4-caddaf&limit=100')
    .then((response) => response.json())
    .then((response) => {
      this.setState({data:response});
      console.log(this.state.data);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  alertItemName = (item) => {
      alert('Status-> ' + item.status)
   }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.container}>
        {
          this.state.data.map((item, index) => (
            <TouchableOpacity
            style = {styles.container}
            onPress = {() => this.alertItemName(item)}>
            <Text style = {styles.text}>
            Vuelo {item.flight.iataNumber}
            </Text>
            </TouchableOpacity>
          ))
        }
        </View>
      </ScrollView>
    );
  }

}

const styles = StyleSheet.create({
  container: {
     padding: 10,
     marginTop: 3,
     backgroundColor: '#d9f9b1',
     alignItems: 'center',
  },
  contentContainer: {
    paddingVertical: 20
  },
  text: {
     color: '#4f603c'
  }
});
