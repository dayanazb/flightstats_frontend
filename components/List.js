import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'

class List extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: this.props.data,
    };
  }

  componentDidMount(){
    console.log(this.props);
  }

  alertItemName = (item) => {
      alert('Status-> ' + item.status)
   }

   render() {
      return (
        <View>
           {
              this.state.data.map((item, index) => (
                 <TouchableOpacity
                    key = {item.aircraft.iataCode}
                    style = {styles.container}
                    onPress = {() => this.alertItemName(item)}>
                    <Text style = {styles.text}>
                       Vuelo {item.flight.iataNumber}
                    </Text>
                 </TouchableOpacity>
              ))
           }
        </View>
      )
   }
}
export default List

const styles = StyleSheet.create ({
   container: {
      padding: 10,
      marginTop: 3,
      backgroundColor: '#d9f9b1',
      alignItems: 'center',
   },
   text: {
      color: '#4f603c'
   }
})
